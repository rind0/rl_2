use std::collections::HashMap;

pub trait Context: std::any::Any + downcast_rs::Downcast + 'static {}
downcast_rs::impl_downcast!(Context);

impl<T> Context for T where T: std::any::Any + 'static {}

pub struct Ref<'a, T: 'static> {
    resource: std::cell::Ref<'a, dyn Context>,
    phantom: std::marker::PhantomData<&'a T>,
}

impl<'a, T: 'static> std::ops::Deref for Ref<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.resource.downcast_ref::<T>().unwrap()
    }
}

pub struct RefMut<'a, T: 'static> {
    resource: std::cell::RefMut<'a, dyn Context>,
    phantom: std::marker::PhantomData<&'a T>,
}

impl<'a, T: 'static> std::ops::Deref for RefMut<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.resource.downcast_ref::<T>().unwrap()
    }
}

impl<'a, T: 'static> std::ops::DerefMut for RefMut<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.resource.downcast_mut::<T>().unwrap()
    }
}

#[derive(Default)]
pub struct Storage {
    stored: HashMap<std::any::TypeId, std::cell::RefCell<Box<dyn Context>>>,
}

impl Storage {
    pub fn new() -> Self {
        Self {
            stored: HashMap::new(),
        }
    }

    pub fn insert<T: std::any::Any>(&mut self, v: T) {
        self.stored.insert(
            std::any::TypeId::of::<T>(),
            std::cell::RefCell::new(Box::new(v)),
        );
    }

    pub fn fetch<T: std::any::Any + 'static>(&self) -> Option<Ref<T>> {
        let clone = match self.stored.get(&std::any::TypeId::of::<T>()) {
            Some(v) => v,
            None => return None,
        };
        Some(Ref {
            resource: std::cell::Ref::map(clone.borrow(), Box::as_ref),
            phantom: std::marker::PhantomData,
        })
    }

    pub fn fetch_mut<T: std::any::Any + 'static>(&self) -> Option<RefMut<T>> {
        let clone = match self.stored.get(&std::any::TypeId::of::<T>()) {
            Some(v) => v,
            None => return None,
        };
        Some(RefMut {
            resource: std::cell::RefMut::map(clone.borrow_mut(), Box::as_mut),
            phantom: std::marker::PhantomData,
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn fetch_primitive_storage() {
        let mut storage = resource::Storage::default();
        storage.insert(1);
        let v = storage.fetch::<i32>().expect("resource is not exists");
        assert_eq!(*v, 1);
    }

    #[test]
    fn fetch_mut_primitive_storage() {
        let mut storage = resource::Storage::default();
        storage.insert(2);
        let mut v = storage.fetch_mut::<i32>().expect("resource is not exists");
        *v = 3;
        assert_eq!(*v, 3);
    }

    #[derive(Debug, PartialEq)]
    struct Unit(i32);

    #[test]
    fn fetch_struct_storage() {
        let mut storage = resource::Storage::default();
        storage.insert(Unit(5));
        let v = storage.fetch::<Unit>().expect("resource is not exists");
        assert_eq!(*v, Unit(5));
    }

    #[test]
    fn fetch_mut_struct_storage() {
        let mut storage = resource::Storage::default();
        storage.insert(Unit(5));
        let mut v = storage.fetch_mut::<Unit>().expect("resource is not exists");
        *v = Unit(10);
        assert_eq!(*v, Unit(10));
    }
}
